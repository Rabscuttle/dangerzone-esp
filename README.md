# Danger Zone ESP
onetap ESP showing text at locations for: 
loot crates, drones, turrets, safes, cash and more.

Created by **Rabscuttle** 
[[GitLab]](https://gitlab.com/Rabscuttle) 
[[Onetap]](https://www.onetap.com/members/rabscuttle.280029)

Based on a script by [Aviarita](https://github.com/Aviarita)


## Setup
1. Clone or download the repo
   ```bash
   $ git clone git@gitlab.com:Rabscuttle/dangerzone-esp.git
   ```

2. Download dependencies
   ```bash
   # with yarn
   $ yarn install
   # or with npm
   $ npm install
   ```

### Tasks
Gulp is used to combine and uglify the scripts using the `build` command.  
Which will build the script to the `./dist/` folder.
```bash
$ gulp build
```

The `watch` command is used to build automatically when you make changes:
```bash
$ gulp watch
```

### Compile to scripts dir directly
If you're actively making changes and want to test them right away,  
you can set it so the tasks build the script to another directory as well.  
This can be done as followed:  

1. Make a copy of `.env.example` and name it `.env`.
   ```cmd
   $ COPY ".env.example" ".env"
   ``` 
2. Inside your new `.env` file, add the path to your `scripts` directory after `DIST=`.
   ```env
   DIST=C:\Program Files (x86)\Steam\steamapps\common\Counter-Strike Global Offensive\ot\scripts
   ```


## TODO
- Show which upgrade
- Show what's inside Lootcrate
- Add knife to CMelee itemIndex switch
