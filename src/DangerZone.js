/**
 * Handles DangerZone entities using `Entity`.
 */
const DangerZone = (function()
{
    var EnabledClassIds = []
    /**
     * Filled by RefreshEntities
     * @type {DZEntity()[]}
     */
    var Entities = []

    /**
     * Fills the Entities property with all loaded entities
     * of the given class id.
     * @param {number[]} classIds
     * @param {function} callback ran after finished
     */
    const RefreshEntities = function(classIds, callback)
    {
        const dzEntities = []
        const entities = Entity.GetEntities()
        // for each loaded entity
        for(var i=0; i<entities.length; ++i)
        {
            const id = Entity.GetClassID(entities[i])
            // if entity is of enabled Class Id
            if(classIds.indexOf(id) > -1)
            {
                // create dzEntity using entity
                var dzEntity = _CreateDZEntity(entities[i], id)
                if(dzEntity != null)
                    dzEntities.push(dzEntity)
            }
        }

        Entities = dzEntities
        callback(dzEntities)
    }

    /**
     * Create a DZEntity with a given normal entity
     * @param {Entity} entity
     * @param {Number} id ClassId
     * @returns {null|DZEntity()}
     * @private
     */
    const _CreateDZEntity = function(entity, id)
    {
        const purple = [210,0,220, 255]
        const yellow = [255,215,0, 255]
        const blue = [30,144,255, 255]
        const green = [6,162,10, 255]
        const white = [255,255,255, 255]
        const red = [255,0,0, 255]

        var name = "Entity"
        var colour = white
        var hOwner = Entity.GetProp(entity, "DT_BaseEntity", "m_hOwnerEntity")
        switch(id)
        {
            // CBRC4Target
            case 27:
                name = "Safe"
                colour = yellow
                break
            // CBreachCharge
            case 28:
                name = "Breach Charge"
                colour = white
                break
            // CBreachChargeProjectile
            case 29:
                name = "Breach Charge (Placed)"
                colour = white
                break
            // CBumpMine
            case 32:
                name = "Mine"
                colour: red
                break
            // CDrone
            case 49:
                name = "Drone"
                colour = white
                break
            // CDronegun
            case 50:
                const health = Entity.GetProp(entity, "DT_Dronegun", "m_iHealth")
                name = "Turret ("+health+"HP)"
                colour = red
                break
            // CItem_Healthshot
            case 104:
                if(hOwner != "m_hOwnerEntity")
                    return null
                name = "Healthshot"
                colour = yellow
                break
            // CItemCash
            case 105:
                name = "Cash"
                colour = green
                break
            // CMelee
            case 112:
                name = "Melee"
                colour = white
                const itemIndex = Entity.GetProp(entity, "DT_BaseCombatWeapon", "m_iItemDefinitionIndex")
                switch(itemIndex)
                {
                    case 75:
                        name = "Axe"
                        break
                    case 76:
                        name = "Hammer"
                        break
                    case 78:
                        name = "Wrench"
                        break
                }
                break
            // CPhysPropAmmoBox
            case 125:
                name = "Ammo Box"
                colour = blue
                break
            // CPhysPropLootCrate
            case 126:
                const entityName = Entity.GetName(entity)
                const index = Entity.GetProp(entity, "DT_PhysPropLootCrate", "m_nModelIndex")
                name = "Loot Crate_" + entityName + "_" + index
                colour = blue
                switch(Entity.GetName(entity))
                {
                    case 821:
                        name = "Airdrop"
                        colour = purple
                        break
                    case 861:
                        name = "Melee crate"
                        colour = white
                        break
                    case 887:
                        name = "Pistol crate"
                        colour = purple
                        break
                    case 913:
                        name = "Explosives crate"
                        colour = blue
                        break
                    case 939:
                        name = "Heavy crate"
                        colour = purple
                        break
                    case 982:
                        name = "Light crate"
                        colour = blue
                        break
                    case 1021:
                        name = "Dufflebag"
                        colour = green
                        break
                }
                break
            // CPhysPropWeaponUpgrade
            case 128:
                name = "Upgrade"
                colour = yellow
                const modelIndex = Entity.GetProp(entity, "DT_BaseEntity", "m_nModelIndex")
                switch (modelIndex)
                {
                    case 1030:
                        name = "Kevlar"
                        break
                    case 1031:
                        name = "Full kevlar"
                        break
                    case 1032:
                        name = "Helmet"
                        break
                    case 1033:
                        name = "Parachute"
                        break
                    case 1034:
                        name = "Briefcase"
                        break
                    case 1035:
                        name = "Zone intel upgrade"
                        break
                    case 1036:
                        name = "Drone upgrade"
                        break
                    case 1038:
                        name = "Exojump suit"
                        break
                }
                break
            // CTablet
            case 172:
                if(hOwner != "m_hOwnerEntity")
                    return null
                name = "Tablet"
                colour = white
                break
        }
        return DZEntity(entity, name, colour)
    }

    return {
        EnabledClassIds,
        Entities,
        RefreshEntities
    }
})