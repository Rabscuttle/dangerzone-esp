/**
 * A model for a DangerZone entity containing the Entity, Name and Colour
 */
const DZEntity = (function(entity, name, colour)
{
    /**
     * @type {Entity}
     */
    var Entity
    /**
     * @type {string}
     */
    var Name
    /**
     * @type {number[]}
     */
    var Colour

    /**
     * @param {Entity} entity
     * @param {string} name
     * @param {number[]} colour
     * @constructor
     */
    const Constructor = function(entity, name, colour)
    {
        Entity = entity
        Name = name
        Colour = colour
    }(entity, name, colour)

    return {
        Entity,
        Name,
        Colour
    }
})
