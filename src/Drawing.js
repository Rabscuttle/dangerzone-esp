/**
 * Handles drawing on the screen with `Render`
 */
const Drawing = (function()
{
    /**
     * Draw a given entity on the screen
     * @param {Entity} entity
     * @param {string} text
     * @param {number[]} colour
     * @param {number} maxDistance
     * @param {boolean} shouldDrawDistance if the distance should be shown
     * @returns {boolean} If drawed succesfully
     */
    const DrawEntityOnScreen = function(entity, text, colour, maxDistance, shouldDrawDistance)
    {
        // get distance to entity
        const player = Entity.GetLocalPlayer()
        const playerPosition = Entity.GetRenderOrigin(player)
        const entityPosition = Entity.GetRenderOrigin(entity)
        const distanceToEntity = _GetMetricDistance(playerPosition, entityPosition)
        if(distanceToEntity > maxDistance)
            return false

        // get draw position on screen
        const entityScreenPosition = Render.WorldToScreen(entityPosition)
        if(!entityScreenPosition)
            return false
        const xPos = entityScreenPosition[0]
        const yPos = entityScreenPosition[1]

        // draw text
        _DrawString(text, colour, xPos, yPos, true)
        // if distance should be drawn, draw it below the text
        if(shouldDrawDistance)
            _DrawString(distanceToEntity+"M", colour, xPos, yPos + 10, false)

        return true
    }

    /**
     * Draw string at given position on screen
     * @param {string} text
     * @param {number[]} colour
     * @param {number} x
     * @param {number} y
     */
    const _DrawString = function(text, colour, x, y, shadow)
    {
        const font = "tahomabd.ttf"
        const fontSize = Settings.FontSize()
        // draw shadow
        if(shadow === true)
        {
            const shadowFont = Render.GetFont(font, fontSize, true)
            // bottom right shadow
            Render.String(x+1, y+1, 1, text, [0,0,0,255], shadowFont)
            Render.String(x+2, y+2, 1, text, [0,0,0,255], shadowFont)
            // border (thats a lot of strings, prolly not worth it)
            // Render.String(x-1, y, 1, text, [0,0,0,255], shadowFont)
            // Render.String(x-2, y, 1, text, [0,0,0,255], shadowFont)
            // Render.String(x+1, y, 1, text, [0,0,0,255], shadowFont)
            // Render.String(x+2, y, 1, text, [0,0,0,255], shadowFont)
            // Render.String(x, y+1, 1, text, [0,0,0,255], shadowFont)
            // Render.String(x, y+2, 1, text, [0,0,0,255], shadowFont)
            // Render.String(x, y-1, 1, text, [0,0,0,255], shadowFont)
            // Render.String(x, y-2, 1, text, [0,0,0,255], shadowFont)
        }
        // draw text
        const textFont = Render.GetFont(font, fontSize, true)
        Render.String(x, y, 1, text, colour, textFont)
    }

    /**
     * @param {number} a
     * @param {number} b
     * @returns {number}
     */
    const _GetMetricDistance = function(a, b)
    {
        // get distance using 3 dimensions
        const distance3D = Math.sqrt(
            Math.pow(a[0] - b[0], 2)
            + Math.pow(a[1] - b[1], 2)
            + Math.pow(a[2] - b[2], 2)
        )
        // convert to metres
        return Math.floor(distance3D * 0.0254)
    }

    return {
        DrawEntityOnScreen
    }
})()
