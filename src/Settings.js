/**
 * Handles intialising and getting settings set by the user
 */
const Settings = (function()
{
    /**
     *
     */
    /**
     * Checkbox UI Element:
     * is enabled or not
     */
    var IsShownCheckbox
    /**
     * Slider UI Element:
     * How far the ESP renders
     */
    var DistanceSlider
    /**
     * Slider UI Element:
     * Font Size
     */
    var FontSizeSlider
    /**
     * Dropdown UI Element:
     * Which entities should be drawn
     */
    var ShownItemsDropdown

    /**
     * Array of possible
     * @type string[]
     */
    var EntityOptions

    const Init = function(entityOptions)
    {
        EntityOptions = entityOptions
        // create subtab
        UI.AddSubTab(["Visuals", "SUBTAB_MGR"], "Danger Zone")
        const path = ['Visuals', 'Danger Zone', 'Danger Zone']
        // register options
        const isShownCheckboxLabel = "Show Danger Zone items"
        const distanceSliderLabel = "ESP distance limit"
        const shownItemsDropdownLabel = "Items to Show"
        const fontSizeSliderLabel = "Font Size"
        IsShownCheckbox = UI.AddCheckbox(path, isShownCheckboxLabel)
        ShownItemsDropdown = UI.AddMultiDropdown(path, shownItemsDropdownLabel, EntityOptions)
        DistanceSlider = UI.AddSliderInt(path, distanceSliderLabel, 1, 500)
        FontSizeSlider = UI.AddSliderInt(path, fontSizeSliderLabel, 5, 20)
        // set default values
        UI.SetValue(path.concat([isShownCheckboxLabel]), 1)
        UI.SetValue(path.concat([distanceSliderLabel]), 250)
        UI.SetValue(path.concat([fontSizeSliderLabel]), 11)
        // if there's 5 options, this will be set to "11111" meaning all of them are enabled (five 1s)
        UI.SetValue(path.concat([shownItemsDropdownLabel]), parseInt("1".repeat(entityOptions.length), 2))
    }

    /**
     * @returns {boolean}
     */
    const IsEnabled = function()
    {
        return UI.GetValue(IsShownCheckbox) != 0
    }

    /**
     * returns all enabled options by name
     * @returns {string[]}
     */
    const ShownItems = function()
    {
        const enabledBinary = UI.GetValue(ShownItemsDropdown).toString(2)
        // loop through binary backwards
        // because if its 00101
        // then index 0 and index 2 of EntityOptions are enabled
        const enabledOptions = []
        for(var i=enabledBinary.length-1; i>=0; --i)
        {
            if(enabledBinary[i] == 1)
            {
                const index = enabledBinary.length - i - 1
                enabledOptions.push( EntityOptions[index] )
            }
        }

        return enabledOptions
    }

    /**
     * get max distance for entities to be drawn
     * @returns {number}
     */
    const MaxDistance = function()
    {
        return UI.GetValue(DistanceSlider)
    }

    /**
     * Get font size
     * @returns {number}
     */
    const FontSize = function()
    {
        return UI.GetValue(FontSizeSlider)
    }

    return {
        Init,
        IsEnabled,
        ShownItems,
        MaxDistance,
        FontSize
    }
})()