// Init modules
const dangerZone = DangerZone()
// TODO: ID's shouldn't have to be defined in two locations
const entityOptions = {
    "Loot Crates": [126],
    "Upgrades": [128],
    "Turrets": [50],
    "Mines": [32],
    "Safes": [27],
    "Cash": [105],
    "Ammo Boxes": [125],
    "Healthshots": [104],
    "Breach Charges": [28, 29],
    "Drones": [49],
    "Melee Weapons": [112],
    "Tablets": [172]
}
Settings.Init(Object.keys(entityOptions))

/**
 * On draw callback
 */
function onDraw()
{
    // cheat must be enabled
    if(!Settings.IsEnabled())
        return

    // get enabled entity Ids
    const enabledNames = Settings.ShownItems()
    var enabledIds = []
    for(var i=0; i<enabledNames.length; ++i)
    {
        const enabledName = enabledNames[i]
        enabledIds = enabledIds.concat(entityOptions[enabledName])
    }
    // get newest (enabled) entities
    dangerZone.RefreshEntities(enabledIds, function(entities)
    {
        // draw each entity
        const maxDistance = Settings.MaxDistance()
        for(var i=0; i<entities.length; ++i)
        {
            const entity = entities[i].Entity
            const text = entities[i].Name
            const colour = entities[i].Colour
            Drawing.DrawEntityOnScreen(entity, text, colour, maxDistance, true)
        }
    })
}
Global.RegisterCallback("Draw", "onDraw")
