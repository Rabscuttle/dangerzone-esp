const {task, watch, src, dest, series} = require('gulp')
const order = require('gulp-order')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify')

require('dotenv').config()

const files = ["src/*.js", "src/**/*.js"]
const filesOrder = [
    "Drawing.js",
    "DZEntity.js",
    "DangerZone.js",
    "Settings.js",
    "main.js"
]

task('build', function()
{
    var build = src(files)
        .pipe(order(filesOrder))
        .pipe(concat('rabscuttle_dangerzone.js'))

    // build not-uglified to extra dist path if given
    const distPath = process.env.DIST
    if(distPath !== undefined)
        build = build.pipe(dest(distPath))

    return build.pipe(uglify())
        .pipe(dest('./dist'))


})

task('watch', function()
{
    watch(files, series('build'))
})
